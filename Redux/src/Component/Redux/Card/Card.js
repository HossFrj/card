import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addToCard, deleteCard, dicCount, editCard, incCount, resCount, showEdit} from '../../Action/cardSlice'
import './Card.css'

const Card = () => {
    const cards = useSelector((reduser) => reduser.card.cards)
    const newCards = useSelector((reduser) => reduser.card.cards)
    const count = useSelector((redus) => redus.card.count)
    const newCount = useSelector((redus) => redus.card.count)
    const [add, setAdd] = useState('')
    const [chCount, setChCount] = useState()
    const dispatch = useDispatch()
    const [showEdit, setShowEdit] = useState()
    const [newAdd, setNewAdd] = useState('')


    const reset = () => {
        dispatch(
            resCount(count)
        )
    }
    const addCard = () => {
        dispatch(
            addToCard({
                id: Math.floor(Math.random() * 100),
                value: add,
                count: count
            }),
        )
        setAdd('')
        reset()

    }
    const inc = () => {
        dispatch(
            incCount(count)
        )
    }
    const dic = () => {
        dispatch(
            dicCount(count)
        )
    }
    const del = (id) => {
        dispatch(
            deleteCard(id)
        )
    }
    const newDic = () => {
        dispatch(
            dicCount(newCount)
        )
    }
    const newInc = () => {
        dispatch(
            incCount(newCount)
        )
    }
    const newReset = () => {
        dispatch(
            resCount(newCount)
        )
    }
    const Edit = (id) => {
        dispatch(
            editCard({

                id: Math.floor(Math.random() * 100),
                value: newAdd,
                count: newCount
            }),


    )
        setNewAdd('')
        newReset()
    }
    // setChCountAdd(count)
    return (
        <div className={'cardpage'}>
            <div className={'input-button-add'}>
                <input value={add} className={'input-add'} onChange={(e) => setAdd(e.target.value)}/>
                <button onClick={addCard} className={'button-add'}>add</button>
                <button onClick={inc} className={'button-inc-add'}>+</button>
                {count}
                <button onClick={dic} className={'button-dic-add'}>-</button>

            </div>
            <div>
                <ul>
                    {cards.map((list) => {
                        return (
                            <div>
                                <li key={list.id}>
                                    {list.value} {list.count}

                                </li>
                                <button onClick={() => del(list.id)}>delete</button>
                                <button onClick={() => setShowEdit(list.id)}>Edit</button>
                                {showEdit === list.id ?
                                    (
                                        <div>
                                            <input value={newAdd} onChange={(e) => setNewAdd(e.target.value)}/>
                                            <button onClick={Edit}>Update</button>
                                            <button onClick={newInc}>+</button>
                                            {newCount}
                                            <button onClick={newDic}>-</button>

                                            {/*{newCards.map((newList)=>{*/}
                                            {/*    return(*/}
                                            {/*        <ul>*/}
                                            {/*            <li>*/}
                                            {/*                {newList.newValue} {newList.newCount}*/}
                                            {/*            </li>*/}
                                            {/*        </ul>*/}
                                            {/*    )*/}
                                            {/*})*/}
                                            {/*}*/}
                                        </div>
                                    ) : null


                                }

                            </div>

                        )


                    })}
                </ul>
            </div>
        </div>
    );
};


export default Card;