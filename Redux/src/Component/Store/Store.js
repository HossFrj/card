import { configureStore } from '@reduxjs/toolkit'
import counterReducer  from "../Action/counterSlice";
import todoSlice from "../Action/todoSlice";
import cardSlice from "../Action/cardSlice";

export default configureStore({
    reducer: {
        counter : counterReducer,
        todo : todoSlice,
        card : cardSlice

    },
})