import React, {useState} from 'react';
import './Cart.css'

function Cart(props) {
    const [add, setAdd] = useState("")
    const [tasks, setTasks] = useState([])
    const [showEdit, setShowEdit] = useState(-1);
    const [updatedText, setUpdatedText] = useState("");
    const [addSelect, setAddSelect] = useState("Litr")
    const [increase, setIncrease] = useState(1)
    const [updateInc, setUpdateInc] = useState(1)
    const [updateSelect, setUpdateSelect] = useState("Litr")



    function getalue(e) {
        setAdd(e.target.value)
    }

    function addToSelect(e) {
        setAddSelect(e.target.value)
    }
    function updateToSelect(e) {
        setUpdateSelect(e.target.value)
    }

    function inc() {
        if (increase>=0){
            setIncrease(increase + 1)
        }else {}
    }

    function dec() {
        if(increase>1){
            setIncrease(increase - 1)
        }

        else { }

    }
    function incUpdate (){
        if(updateInc>=0){
            setUpdateInc(updateInc + 1)
        }else {

        }
    }
    function decUpdate (){
        if(updateInc>1){
            setUpdateInc(updateInc - 1)
        }else {

        }

    }


    function buttonVale(e) {
        tasks.map((iii) => {
            if (iii.value === add) {
                const newArray = tasks.filter((del) => del.value !== add)
                setTasks(newArray)
                alert("repetitious!!! please enter new value again")
                // console.log(tasks)
            } else {

            }
        })
        if (add.length > 10) {
            alert("should be input value length < 10")
        } else {
            if (add === "") {
            } else {

                // let tmp = [...tasks]
                // tmp.push(add)
                // setTasks(tmp)
                //  setTasks(prev => [...tasks, add])
                const item = {
                    id: Math.floor(Math.random() * 1000),
                    value: add,
                    count: " " + addSelect,
                    num: " " + increase
                };

                setTasks((oldList) => [...oldList, item]);
                // setShowSelect((list)=>[...list,sBox])
                setAdd("")
                setIncrease(1)


            }
        }
    }

    function deletee(id) {
        // console.log(id)
        // console.log(tasks)
        const newArray = tasks.filter((del) => del.id !== id)
        setTasks(newArray)
    }

    function editItem(id, newText, con, valN) {
        tasks.map((iii) => {
            if (iii.value === updatedText) {
                alert("repetitious!!! please enter new value again")
                return newText = ""

            }
        })

        if (newText.length > 10) {
            alert("should be input value length < 10")
        } else {

            if (newText === "") {

            } else {
                const currentItem = tasks.filter((item) => item.id === id);
                const newItem = {
                    id: currentItem.id,
                    value: newText,
                    count: " "+updateSelect,
                    num: " "+updateInc
                };
                deletee(id);
                setTasks((oldList) => [...oldList, newItem]);
                setUpdatedText("")
                setUpdateInc(1)
                setShowEdit(-1);
            }
        }
    }

    const options = [
       {label: " Liter ", value: " Liter ",}, {label: "Kilo", value: " Kilo",},
        {label: "Pieces ", value: " Pieces ",}];


    return (
        <div className={"mainQQ"}>
            <div className={"mainTD"}>
                <div className={"divOnIBS"}>
                    <div className={"divIB"}>
                        <input value={add} onChange={getValue} type={"text"} className={"input1"}/>
                        <div className={"btnIncDic"}>
                            <button className={"btnInc"} onClick={inc}>+</button>
                            <span className={"span01"}>{increase}</span>
                            <button className={"btnDec"} onClick={dec}>-</button>
                        </div>
                        <select className={"selectAdd"} onChange={addToSelect}>
                            {options.map((option) => (
                                <option value={option.value}>{option.label}</option>
                            ))}
                        </select>
                        <button onClick={buttonVale} type={"submit"} className={"button1"}>Add</button>
                    </div>
                </div>
                <div className={"divCN"}>
                    <ul>
                        {tasks.map((item) => {
                            return (
                                <div>
                                    <li className={'li'} key={item.id}>
                                        {item.value}
                                        {item.num}
                                        {item.count}
                                        <div>
                                            <button className="buttonDel" onClick={() => deletee(item.id)}>Delete
                                            </button>
                                            <button className={"buttonDel"} onClick={() => setShowEdit(item.id)}>Edit
                                            </button>
                                        </div>

                                    </li>
                                    {showEdit === item.id ? (
                                        <div>
                                            <input className={"inputUpdate"} type="text" value={updatedText}
                                                   onChange={(e) => setUpdatedText(e.target.value)}/>
                                            <select className={"selectAdd"} onChange={updateToSelect} >
                                                {options.map((option) => (
                                                    <option onClick={()=>editItem(item.id, updateSelect)} value={option.value}>{option.label}</option>
                                                ))}
                                            </select>
                                            <button className={"buttonDel"}
                                                    onClick={() => editItem(item.id, updatedText)}>Update
                                            </button>
                                            <div className={"btnIncDicEdit"}>
                                                <button className={"btnIncEdit"} onClick={incUpdate}>+</button>
                                                <span onClick={()=>editItem(item.id,updateInc)}  className={"span0"}>{updateInc}</span>
                                                <button className={"btnDecEdit"} onClick={decUpdate}>-</button>
                                            </div>
                                        </div>
                                    ) : null}

                                </div>

                            );

                        })}
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default Cart;