import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Home from "./Component/Task/Home/Home";
import Hov from "./Component/Task/Hover/Hov";
import Click from "./Component/Task/Click/Click";
import Counter from "./Component/Task/Counter/Counter";
import Card from "./Component/Task/Cart/Card";
import Todo from "./Component/Task/Todo6/Todo";
import Timer from "./Component/Task/Timer/Timer";
import LayLay from "./Component/Task/SiteBank/Layout";

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(

  <BrowserRouter>
      <Routes>
      <Route element={<LayLay/>} path={'/'} />
      </Routes>
      {/*<Routes>*/}
      {/*<Route element={<Home/>} path={'/'}/>*/}
      {/*    <Route element={<Hov/>} path={'/1'}/>*/}
      {/*    <Route element={<Click/>} path={'/2'}/>*/}
      {/*    <Route element={<Counter/>} path={'/3'}/>*/}
      {/*    <Route element={<Card/>} path={'/4'}/>*/}
      {/*    <Route element={<Todo/>} path={'/5'}/>*/}
      {/*    <Route element={<Timer/>} path={'/6'}/>*/}


      {/*</Routes>*/}
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
